import { element } from 'encode-html-template-tag';

const isFunction = f => (typeof f === 'function');

/**
 * A representation of a form element, for rendering a form
 *
 * @typedef FormElement
 * @param {Function} render	Generate the element as a string
 * @param {Function} parse	Get the form data from a request object
 */

/**
 * Creates a form element.
 * @param {Object}	attributes	Dict of attributes to add to the form element
 * @param {*}	body	Body of the form
 * @param {Function}	parser	Function to transform the request body
 * @returns {FormElement}
 */
function form(attributes, body, parseParams) {
	if(!isFunction(parseParams) && (isFunction(body) || !body)) {
		[attributes, body, parseParams] = [{}, attributes, body];
	}

	const f = element(
		'form',
		{
			method: 'POST',
			...attributes,
		},
		body
	);
	f.parse = parser(parseParams);
	return f;
}

export default form;

/**
 * Parse a request and return a URLSearchParams object
 * @param {http.IncomingMessage} req The request object to parse
 * @returns {URLSearchParams}
 */
export async function parse(req) {
	if(req.method === 'POST') {
		const streamBody = await readStream(req);
		return new URLSearchParams(streamBody);
	} else {
		return new URL('path:'+req.url).searchParams;
	}
}

async function readStream(readable) {
	readable.setEncoding('utf8');
	let data = '';
	for await (const chunk of readable) {
		data += chunk;
	}
	return data;
}

/**
 * Create a function for parsing form values from a request object.
 * The new function will recieve the request object and retrieve the submitted form values
 * as URLSearchParams.
 * You can then supply a function for transforming these params before returning them.
 * @param {Function} parseParams A function that accepts URLSearchParams and returns the parsed form values
 */
export const parser = parseParams => async function(req) {
	const params = await parse(req);
	return parseParams ? parseParams(params) : params;
};
