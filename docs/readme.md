# html-form-component

Little library to generate an HTML form and parse the request it submits.
Based on html-encode-template-tag.

```javascript
import html from 'encode-html-template-tag';
import form from 'html-form-component';

const nameForm = form(
	{
		method: 'POST'
	},
	html`<label>
		What's your name?
		<input name="name">
	</label>
	<button>Submit</button>
	`,
	params => params.get('name')
);

export default async (req, res) => {
	if(req.method==='POST') {
		const name = await nameForm.parse(req);

		res.end(await html`Your name is ${name}`.render());
		return;
	}

	res.end(await nameForm.render());
}
```
