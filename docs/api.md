
## Constants

<dl>
<dt><a href="#parser">parser</a></dt>
<dd><p>Create a function for parsing form values from a request object.
The new function will recieve the request object and retrieve the submitted form values
as URLSearchParams.
You can then supply a function for transforming these params before returning them.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#form">form(attributes, body, parser)</a> ⇒ <code><a href="#FormElement">FormElement</a></code></dt>
<dd><p>Creates a form element.</p>
</dd>
<dt><a href="#parse">parse(req)</a> ⇒ <code>URLSearchParams</code></dt>
<dd><p>Parse a request and return a URLSearchParams object</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#FormElement">FormElement</a></dt>
<dd><p>A representation of a form element, for rendering a form</p>
</dd>
</dl>

<a name="parser"></a>

## parser
Create a function for parsing form values from a request object.
The new function will recieve the request object and retrieve the submitted form values
as URLSearchParams.
You can then supply a function for transforming these params before returning them.



| Param | Type | Description |
| --- | --- | --- |
| parseParams | <code>function</code> | A function that accepts URLSearchParams and returns the parsed form values |

<a name="form"></a>

## form(attributes, body, parser) ⇒ [<code>FormElement</code>](#FormElement)
Creates a form element.



| Param | Type | Description |
| --- | --- | --- |
| attributes | <code>Object</code> | Dict of attributes to add to the form element |
| body | <code>\*</code> | Body of the form |
| parser | <code>function</code> | Function to transform the request body |

<a name="parse"></a>

## parse(req) ⇒ <code>URLSearchParams</code>
Parse a request and return a URLSearchParams object



| Param | Type | Description |
| --- | --- | --- |
| req | <code>http.IncomingMessage</code> | The request object to parse |

<a name="FormElement"></a>

## FormElement
A representation of a form element, for rendering a form



| Param | Type | Description |
| --- | --- | --- |
| render | <code>function</code> | Generate the element as a string |
| parse | <code>function</code> | Get the form data from a request object |

