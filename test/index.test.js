import form from '../src/index.js';
import { jest } from '@jest/globals';
import { Readable } from 'stream';

test('Generates form', async ()=>{
	expect(await form('Body', x=>x).render()).toMatchSnapshot();
	expect(await form({ method: 'GET' }, 'Body', x=>x).render()).toMatchSnapshot();
});

test('Parses post request', async ()=>{
	const parser = jest.fn(x=>x.get('name'));
	const req = Readable.from(['name=paul']);
	req.setEncoding = jest.fn();
	req.method = 'POST';

	const f = form('b', parser);
	expect(await f.parse(req)).toEqual('paul');
});


test('Parses get request', async ()=>{
	const parser = jest.fn(x=>x.get('name'));
	const req = {
		setEncoding: jest.fn(),
		url: '/path/?name=laura'
	};

	const f = form('b', parser);
	expect(await f.parse(req)).toEqual('laura');
});

test('Form with no parser', async ()=>{
	const f = form({}, 'No parser');
	expect(await f.render()).toMatchSnapshot();

	const f2 = form('No attrs no parser');
	expect(await f2.render()).toMatchSnapshot();

	const req = {
		setEncoding: jest.fn(),
		url: '/path/?animal=birds'
	};

	expect(await f2.parse(req)).toBeInstanceOf(URLSearchParams);
});
