# Create empty file
echo "" > docs/api.md
# Generate docs and trim trailing whitespace and useless stuff
./node_modules/.bin/jsdoc2md src/index.js | sed 's/[ \t]*$//;s/\*\*Kind\*\*: global [^\n]*//' >> docs/api.md

cat docs/readme.md docs/api.md > README.md
